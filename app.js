
let express = require('express'),
    app = express(),
    io = require('socket.io'),
    http = require('http'),
    bodyParser = require('body-parser'),
    server = http.createServer(app),
    request = require('request');

const BASEURL = 'http://gocloudasiaacademe.com/phinma-student-api-test/Requests/';

io = io.listen(server, {
        pingTimeout: 2000,
        pingInterval: 4000
    });

app.use(express.static(__dirname + '/node_modules'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE, OPTIONS');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
});





app.get('/getRequest', (req, resp) => {
    let url = BASEURL+'getAvailableRequest?school_code=jocson';

    request.get(url, (err, res) => {
    if (err) { return console.log(err); }
        // resp.json(res.body);
       resp.json(JSON.parse(res.body));
    });
});

app.get('/getStudentRequest', (req, resp) => {
    let url = BASEURL+'getRequests/enrollment_id/5717/school_code/jocson';

    request.get(url, (err, res) => {
        if(err) { console.log(err); }
        resp.json(JSON.parse(res.body));
    })

})

app.post('/saveRequest', (req, resp) => {
    let url = BASEURL+'saveRequest';
    // resp.json(req.body);
    request.post({
        url : url,
        form : req.body,
        json: true,
    }, (err, res) => {
    if (err) { return console.log(err); }
        resp.json(res.body);
        console.log(res.body);
    //    resp.json(JSON.parse(res.body));

    });
});

io.on('connection', (client) => {
    console.log('User Connected');
    client.emit('connection-status' , 'Connected to server');

    client.on('send offline', (data) => {
        client.broadcast.emit('offline data receive', 'Data Receive');
    });

    client.on('disconnect', () => {
        client.emit('disconnect-status', 'Disconnected From Server');
    });
});

server.listen(3300, function(){
    console.log('Server started');
})